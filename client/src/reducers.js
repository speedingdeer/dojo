import { combineReducers } from 'redux';
import { default as api } from './api/reducer';

const reducers = combineReducers({
  api,
});

export default reducers;