import React, { Component } from 'react';
import { Table, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types'

import './Home.css';

const PAGE_SIZE = 5;

class Home extends Component {

  hero_attrs = [
    { key: 'name', label: 'Name' },
    { key: 'id', label: 'Id' },
    { key: 'image_portrait', label: 'Portrait' },
    { key: 'image_portrait', label: 'Splash' },
    { key: 'image_card_background', label: 'Card Background' },
    { key: 'links', label: 'Links' },
    { key: 'relationships', label: 'Relationship' }
  ]

  getAttr(hero, attr) {
    if(hero[attr]) {
      if(hero[attr] instanceof Object) {
        return JSON.stringify(hero[attr]);
      }
      if(/.+\.(jpg|png)$/.test(hero[attr])) {
        return (<Image src={hero[attr]} size='small'/>)
      }
      return hero[attr]

    } else {
      return '?';
    }
  }

  render() {
    return (
      <div className='home' ref={(c) => this.elem = c}>
        <Table striped>
          <Table.Header>
            <Table.Row>
              {this.hero_attrs.map( (a, i) =>
                <Table.HeaderCell key={i}>
                  {a.label}
                </Table.HeaderCell>
              )}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.props.heros.map( (h, i) =>
              <Table.Row key={i}>
                {this.hero_attrs.map( (a, ai) =>
                  <Table.Cell key={ai}>
                  {this.getAttr(h, a.key)}
                </Table.Cell>
                )}
              </Table.Row>
            )}
          </Table.Body>
        </Table>
      </div>
    );
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll.bind(this));
    this.elem.addEventListener('scroll', this.onScroll.bind(this));
    this.onScroll();
  }

  onScroll(evt) {
    if(!this.elem) return;
    let rect = this.elem.getBoundingClientRect();
    if(rect.height + rect.y - window.innerHeight < 200 && !this.props.fetching && this.props.has_more) {
      this.props.loadData({
        page: this.props.heros.length / PAGE_SIZE,
        page_size: PAGE_SIZE
      })
    }
  }


}

Home.propTypes = {
  loadData: PropTypes.func.isRequired,
  heros: PropTypes.array.isRequired,
  fetching: PropTypes.bool.isRequired,
  has_more: PropTypes.bool.isRequired
}

export default Home;