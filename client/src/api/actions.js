import api from './index'


/*****  API GET *****/

export const GET_ENTITIES = 'GET_ENTITIES';
export const GOT_ENTITIES = 'GOT_ENTITIES';

function _get(endpoint, q, mount) {
  return {
    type: GET_ENTITIES,
    endpoint,
    q,
    mount
  }
}

function _got(endpoint, q, res, mount) {
  return {
    type: GOT_ENTITIES,
    endpoint,
    q,
    data: res.data,
    receivedAt: Date.now(),
    mount
  }
}

export function got(endpoint, res, mount = null,  q = {}) {
  if (mount === null) { mount = endpoint; }
  return _got(endpoint, q, res, mount);
}

export function get(endpoint, q = {}, mount = null) {

  if (mount === null) { mount = endpoint; }
  return function (dispatch) {
    dispatch(_get(endpoint, q, mount))
      api.get(`/${endpoint}`, q)
        .then(res => {
          dispatch(_got(endpoint, q, res, mount))
        }).catch(e => {
          console.log(e);
          // @TODO:
          // dispatch error
        });
  }
}

