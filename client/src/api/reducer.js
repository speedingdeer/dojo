import {
  GET_ENTITIES, GOT_ENTITIES
} from './actions'

export default function(
  // @TODO:
  // it feels right now to split the state on fetching: {} and entities: {}
  state = {},
  action
) {
  switch (action.type) {
    case GET_ENTITIES:
      return Object.assign({}, state, {
        [action.mount]: {
          ...state[action.mount],
          fetching: true
        }
      })
    case GOT_ENTITIES:
      let data = (state[action.mount].data) ? state[action.mount].data : [];
      let has_more = (action.data && action.data.length > 0)
      return Object.assign({}, state, {
        [action.mount]: {
          ...state[action.mount],
          data: data.concat(action.data),
          fetching: false,
          has_more: has_more,
          q: action.q
        }
      })
    default:
      return state
  }
}
