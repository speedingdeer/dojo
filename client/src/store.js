import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reducers from './reducers';

var middlewares = [
  thunkMiddleware // lets us dispatch() functions
]; 
if(process.env.NODE_ENV !== 'production') {
  middlewares.push(createLogger()); // neat middleware that logs actions
}

const store = createStore(
  reducers,
  applyMiddleware(...middlewares)
)

export default store;