import React from 'react';
import PropTypes from 'prop-types'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import Home from './containers/Home';


const Routes = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home}/>
      </Switch>
    </BrowserRouter>
  </Provider>
)



Routes.propTypes = {
  store: PropTypes.object.isRequired
}


export default Routes