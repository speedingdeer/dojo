import React from 'react';
import { render } from 'react-dom';
import Routes from './routes';
import 'semantic-ui-css/semantic.min.css';
import store from './store';

import './index.css'

render(
  <Routes store={store} />,
  document.getElementById('root')
);