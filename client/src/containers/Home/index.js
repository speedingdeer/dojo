import { connect } from 'react-redux'
import { get as api_get } from '../../api/actions'
import { default as HomeComponent } from '../../components/Home'


const mapStateToProps = state => {
  return {
    heros: (state.api.heros && state.api.heros.data) ? state.api.heros.data : [],
    fetching: (state.api.heros && state.api.heros.fetching) ? state.api.heros.fetching : false,
    has_more: (state.api.heros && state.api.heros.has_more === false) ? state.api.heros.has_more : true
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadData(q) {
      dispatch(api_get('heros', q))
    }
  }
}

const Home = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeComponent)

export default Home