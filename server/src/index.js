import app from './app';
import config from './config';
import seed from './config/seed';

(async () => {
  if(config.SEED_DB) { await seed(); }
  app.listen(config.PORT, () => console.log(`Listening on port ${config.PORT}`)); // eslint-disable-line no-console
})();
