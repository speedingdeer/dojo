import path from 'path'

let NODE_ENV = process.env.NODE_ENV || 'dev';

let DEFAULT_CONFIG =  {
  PORT: process.env.PORT || 8888,
    // MongoDB connection options
  mongo: {
    uri: process.env.MONGODB_URI || 'mongodb://localhost/tmp-dev'
  },

  // Seed database on startup
  SEED_DB: true,
  NODE_ENV: NODE_ENV,
  UPLOADS: process.env.UPLOADS || path.join(__dirname, '../../uploads'),
  SESSION_SECRET: process.env.SESSION_SECRET || 'secret'
}

Object.assign(DEFAULT_CONFIG, require(`./enviroments/${NODE_ENV}env`).default);

export default DEFAULT_CONFIG