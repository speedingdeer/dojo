import fs from 'fs';
import path from 'path';
import Hero from '../api/hero/hero.model';

export default async () => {

  await Hero.remove({});

  // read JSON
  let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'data.json'), 'utf8'));
  for(let o of json.data) {    
    let hero = new Hero(
      Object.assign({}, o, {...o.attributes})
    );
    await hero.save();
  }

  console.log(`Database seeded`); // eslint-disable-line no-console

}