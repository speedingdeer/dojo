export default {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/tmp-test'
  },

  // Seed database on startup
  seedDB: false
}