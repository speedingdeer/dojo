import request from 'supertest';
import app from '../../app';
import config from '../../config';
import { expect } from 'chai';

let admin, user, user_token, admin_token;

beforeEach(async () => {
  // @TODO:
  // create some heros
});


describe('GET /heros', () => {
  it('should list avaiable heros', async () => {
    await request(app).get('/api/heros')
      .expect(200)
      .expect(res =>  {
        // expect(res.body).to.have.length(4);
      })
  });
  it('should process the limit and from params', async () => {
    await request(app).get('/api/heros')
      .expect(200)
      .expect(res =>  {
        // expect(res.body).to.have.length(2);
      })
  });
})

afterEach(async () => {
  // @TODO:
  // remove all heros
});