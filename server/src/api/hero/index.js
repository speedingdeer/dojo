import express from 'express';
import controller from './hero.controller';

let router = express.Router();

router.get('/', controller.index);

module.exports = router;
