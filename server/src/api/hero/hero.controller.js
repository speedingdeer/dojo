import Hero from './hero.model';


module.exports.index = async (req, res, next) => {
  try {
    var page = req.query.page ? Number(req.query.page) : 1;
    var page_size = req.query.page_size ? Number(req.query.page_size) : 5;
    res.json(await Hero.find({}).limit(page_size).skip(page * page_size));
  } catch (e) {
    //this will eventually be handled by your error handling middleware
    next(e) 
  }
}