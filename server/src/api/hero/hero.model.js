'use strict';

const mongoose = require('bluebird').promisifyAll(require('mongoose'));

export default mongoose.model('Hero', new mongoose.Schema({
  // attributes
  name: { type: String, trim: true, required: true },
  slug: { type: String, trim: true, required: true }, // it's probably unique
  image_portrait: { type: String, trim: true, required: true },
  image_splash: { type: String, trim: true },
  image_card_background: { type: String, trim: true },

  id: { type: Number, required: true },

  // @TODO: change them to some nested models or something like this.
  links: Object,
  relationships: Object



}));
